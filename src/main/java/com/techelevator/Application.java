package com.techelevator;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.AWTException;
import java.awt.image.BufferedImage;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class Application {

    public static void main(String[] args) throws AWTException, IOException, TesseractException, InterruptedException {
        // Instantiate helper objects
        Robot robot = new Robot();
        Tesseract tesseract = new Tesseract();
        Keyboard keyboard = new Keyboard(robot);

        // Perform a screen capture of a section of the screen, defined by a Rectangle
        Rectangle rectangle = new Rectangle(10, 10, 1000, 1000);
        BufferedImage partialScreen = robot.createScreenCapture(rectangle);

        // Save it to a file
        ImageIO.write(partialScreen, "png", new File("snapshot.png"));

        // OCR it!
        String output = tesseract.doOCR(partialScreen);

        System.out.println("I just saw this: ");
        System.out.println(output);

        clickOn(robot, 320, 10);
        clickOn(robot, 500, 370);
        keyboard.type("\n");
        keyboard.type("Connection to USA\n");
    }

    private static void clickOn(Robot robot, int x, int y) throws InterruptedException {
        robot.mouseMove(x, y);
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        TimeUnit.SECONDS.sleep(1);
    }
}
